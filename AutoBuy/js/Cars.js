// JavaScript Document
var carItem = {
    results:[],
    search: function(carsArray,keyword,unique){
        for (car of carsArray){
            for (property in car){
                if (car[property]===keyword){
                    this.results.push(car)
                    if (unique===true){
                        return car;
                    }
                }
            }
        }
        return this.results;
    },
    generatePath: function(car){
        var path = car["Images Path"]+car["Number of Images"]+".JPG";
        return path;
    },
    generateTitle: function(car){
        return car["Condition"] + " " + 
                car["Colour"] + " " + 
                car["Year"] + " " + 
                car["Make"] + " " + 
                car["Model"];
    },
    generateFooter: function(car){
        return car["Price"];
        
    },
    generateDescription: function(car){
        var description="";
        
        for (attribute in car){
            if (attribute !== "Source" &&
                    attribute !== "Images Path"&&
                    attribute !== "Number of Images"&&
                    attribute !== "Price"&&
                    attribute !== "StockNumber"
                    ){
                description += attribute + ": ";
                description += car[attribute]+"<br/>";
            }
        }
        return description;
    }
    
    
};
var cars = [
    {
        "Price":"50000$",
        "Condition" : "Used",
        "Year" : "2011",
        "Make" : "Toyota",
        "Model" : "Matrix",
        "Colour" : "Blue",
        "Body Type" : "Hatchback",
        "Drivetrain" : "Other",
        "Transmission" : "Automatic",
        "Fuel Type" : "Other",
        "StockNumber" : "AJ7754E",
        "Kilometers" : "108,000",
        "Engine Capacity" : "1.8L",
        "Engine Type" : "4 Cylindres",
        "City Consumption" : "8.1",
        "Highway Consumption" : "6.3",
        "Source" : "Kijiji",
        "Images Path" : "images/AJ7754E/",
        "Number of Images" : "1"
    },{ 
        "Price":"25995$",
        "Condition" : "New",
        "Year" : "2012",
        "Make" : "Jeep",
        "Model" : "Grand Cherokee",
        "Colour" : "Gold",
        "Body Type" : "SUV, Crossover",
        "Drivetrain" : "4x4",
        "Transmission" : "Automatic",
        "Fuel Type" : "Other",
        "StockNumber" : "17-1342A",
        "Kilometers" : "66000",
        "Engine Capacity" : "5.7L",
        "Engine Type" : "V6",
        "City Consumption" : "13.0",
        "Highway Consumption" : "8.8",
        "Source" : "Kijiji",
        "Images Path" : "images/17-1342A/",
        "Number of Images" : "1"
    },{ 
        "Price":"5950$",
        "Condition" : "Used",
        "Year" : "2006",
        "Make" : "BMW",
        "Model" : "325I",
        "Colour" : "Gray",
        "Body Type" : "Sedan",
        "Drivetrain" : "RWD",
        "Transmission" : "Manual",
        "Fuel Type" : "Gas",
        "StockNumber" : "S1001",
        "Kilometers" : "168,000",
        "Engine Capacity" : "3.0L",
        "Engine Type" : "6 Cylinders Inline",
        "City Consumption" : "13.8",
        "Highway Consumption" : "8.7",
        "Source" : "Kijiji",
        "Images Path" : "images/S1001/",
        "Number of Images" : "1"
    },{ 
        "Price":"52000$",
        "Condition" : "New",
        "Year" : "2018",
        "Make" : "Toyota",
        "Model" : "Celica",
        "Colour" : "Blue",
        "Body Type" : "Hatchback",
        "Drivetrain" : "Other",
        "Transmission" : "Automatic",
        "Fuel Type" : "Other",
        "StockNumber" : "AJ7755E",
        "Kilometers" : "108,000",
        "Engine Capacity" : "1.8L",
        "Engine Type" : "4 Cylindres",
        "City Consumption" : "8.1",
        "Highway Consumption" : "6.3",
        "Source" : "Kijiji",
        "Images Path" : "images/AJ7755E/",
        "Number of Images" : "1"
    },{
        "Price":"94900$",
        "Condition" : "Used",
        "Year" : "2014",
        "Make" : "Maserati",
        "Model" : "Quattroporte S Q4",
        "Colour" : "Black",
        "Body Type" : "Sedan",
        "Drivetrain" : "AWD",
        "Transmission" : "Automatic",
        "Fuel Type" : "Gas",
        "StockNumber" : "14M41A",
        "Kilometers" : "80,000",
        "Engine Capacity" : "3.0L",
        "Engine Type" : "V6",
        "City Consumption" : "14.7",
        "Highway Consumption" : "10.2",
        "Source" : "Kijiji",
        "Images Path" : "images/14M41A/",
        "Number of Images" : "1"
    },{
        "Price":"99999$",
        "Condition" : "New",
        "Year" : "2016",
        "Make" : "Jeep",
        "Model" : "Cherokee Trailhawk",
        "Colour" : "Black Matt",
        "Body Type" : "SUV",
        "Drivetrain" : "4x4",
        "Transmission" : "Automatic",
        "Fuel Type" : "Gas",
        "StockNumber" : "3008",
        "Kilometers" : "6905",
        "Engine Capacity" : "3.2L",
        "Engine Type" : "V6",
        "City Consumption" : "12.4",
        "Highway Consumption" : "9.4",
        "Source" : "Kijiji",
        "Images Path" : "images/3008/",
        "Number of Images" : "1"
    },{
        "Price":"128900$",
        "Condition" : "New",
        "Year" : "2015",
        "Make" : "BMW",
        "Model" : "i8",
        "Colour" : "Metalic",
        "Body Type" : "Coupe",
        "Drivetrain" : "AWD",
        "Transmission" : "Automatic",
        "Fuel Type" : "Electrik",
        "StockNumber" : "B0007",
        "Kilometers" : "4600",
        "Engine Capacity" : "0L",
        "Engine Type" : "Electrik",
        "City Consumption" : "0",
        "Highway Consumption" : "0",
        "Source" : "Kijiji",
        "Images Path" : "images/B0007/",
        "Number of Images" : "1"
    },{
        "Price":"129949",
        "Condition" : "Used",
        "Year" : "2012",
        "Make" : "Maserati",
        "Model" : "Granturismo MC Sport Line",
        "Colour" : "Black",
        "Body Type" : "Coupe",
        "Drivetrain" : "RWD",
        "Transmission" : "Tip-Tronik",
        "Fuel Type" : "Gasoline",
        "StockNumber" : "5G790",
        "Kilometers" : "4600",
        "Engine Capacity" : "4.7L",
        "Engine Type" : "V8",
        "City Consumption" : "18.1",
        "Highway Consumption" : "12.0",
        "Source" : "Kijiji",
        "Images Path" : "images/5G790/",
        "Number of Images" : "1"
    }
];
// I want to show the image of the car