var search = {
    $searchButton:"",
    searchValue:"",
    cookieName:"searchKeyword",
    expiry:";max-age=+10000;",
    
    cacheDOM : function(){
        this.$searchButton = $("#searchButton");
        this.$searchInput = $("#searchInput");
        this.$searchSubmit = $("#btnsubmit");
        this.$maserati = $("#maserati");
        this.$bmw = $("#bmw");
        this.$jeep = $("#jeep");
        
    },
    bindEvents : function(){
        this.$searchButton.click(function(){search.search();});
        this.$searchSubmit.click(function(){search.search();});
        
        $(this.$bmw).click(function(){search.search("BMW");});
        $(this.$maserati).click(function(){search.search("Maserati");});
        $(this.$jeep).click(function(){search.search("Jeep");});
    },
    search : function(forceFeedCookie){
        this.setCookie(forceFeedCookie);
        window.location.replace("compare.html");
    },
    setCookie : function(forceFeedCookie){
        if (!forceFeedCookie){
            this.searchValue = this.$searchInput.val();
            document.cookie = this.cookieName + "=" + this.searchValue + this.expiry;
        }else{
            this.searchValue = forceFeedCookie;
            document.cookie = this.cookieName + "=" + this.searchValue + this.expiry;
        }
    },
    init : function(){
        this.cacheDOM();
        this.bindEvents();
        this.$searchButton.hide();
    },
    getCookie : function() {
        if (document.cookie.length !== 0){
            var nameValueArray = document.cookie.split(";");
            for (var i = 0 ; i < nameValueArray.length ; i++){
                nameValueArray[i] = nameValueArray[i].split("=");
            }
            return nameValueArray;
        }else{
            alert("Cookie not found");
        }
    }
};

$(document).ready(function(){
    search.init();
    
});