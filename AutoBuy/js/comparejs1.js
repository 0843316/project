var compare = {
    $modules:[{},{},{},{}],
    
    init : function(){
        this.loadCookie();
        this.cacheDOM();
        this.searchCars();
        compare.carsSelected = [];
        compare.selectionNumber = 0;
    },
    cacheDOM: function(){
        var $DOM = $(".jumbotron");
        this.$carsDOMArray = $DOM.find(".col-lg-3");
        this.$chose = $($DOM).find("#btnCompare");
        
        
        for (var i = 0 ; i < this.$carsDOMArray.length; i++){
            this.$modules[i]["$image"] = $(this.$carsDOMArray[i]).find(".img-responsive");
            this.$modules[i]["$caption"] = $(this.$carsDOMArray[i]).find(".caption");
            this.$modules[i]["$p"] = $(this.$carsDOMArray[i]).find("p");
            this.$modules[i]["$footer"] = $(this.$carsDOMArray[i]).find("footer");
            this.$modules[i]["$overlayTitle"] = $(this.$carsDOMArray[i]).find(".overlay").find("h2");
            this.$modules[i]["$checkBox"] = $(this.$carsDOMArray[i]).find(".overlay").find("input");
        }
        
        console.log(this.$carsDOMArray);
    },
    searchCars : function(){
        var keyword = this.loadCookie();
        this.carsResult = carItem.search(cars,keyword);
        for (var i=0 ; i < this.$modules.length; i++){
            if (i <  this.carsResult.length ){
                this.renderCar(this.carsResult[i],this.$modules[i]);
            }else{
                this.renderEmpty(i);
            }
        }
    },
    loadCookie : function() {
        console.log("compare.loadCookie()");
        var cookieArray = search.getCookie();
        for (array of cookieArray){
            console.log(array);
            var index = jQuery.inArray("searchKeyword",array);
            var index2 = jQuery.inArray(" searchKeyword",array);
            console.log("the index is "+index);
            console.log("the index2 is "+index2);
            
            if (index !== -1){
                
                console.log(array[index+1]);
                return array[index+1];
            }
            if (index2 !== -1){
                console.log(array[index2+1]);
                return array[index2+1];
            }
        }
    },
    getCookieValueByName : function (cookieName){
        var cookieArray = search.getCookie();
        for (array of cookieArray){
            console.log("compare.getCookieValueByName(cookieName)");
            console.log(array);
            var index = jQuery.inArray(cookieName,array);
            console.log("the index is "+index);
            if (index !== -1){
                
                console.log(array[index+1]);
                return array[index+1];
            }
        }
    },
    renderEmpty: function(i){
        $(this.$carsDOMArray[i]).html("");
        
    },
    renderCar : function(car,carDOM) {
        var imagepath = carItem.generatePath(car);
        var overlayTitle = carItem.generateTitle(car);
        var footer = carItem.generateFooter(car);
        var description = carItem.generateDescription(car);
        
        $(carDOM["$image"]).attr("src",imagepath);
        $(carDOM["$p"]).html(footer);
        $(carDOM["$footer"]).html(description);
        $(carDOM["$overlayTitle"]).html(overlayTitle);
        $(carDOM["$checkBox"]).attr("id",car["StockNumber"]);
        this.cacheDOM();
        this.bindEvents(car);
    },
    bindEvents : function (car){
        var StockNumber = car["StockNumber"];
        for (var i = 0 ; i < this.carsResult.length; i++){
            var appropriateCheck = $(this.$modules[i]["$checkBox"]).attr("id");
            if (appropriateCheck === StockNumber){
                console.log(appropriateCheck);
                console.log("is for the stock numbner"+StockNumber);
                $(this.$modules[i]["$checkBox"]).change(function(){compare.selectCar(car);});
            }
            
        }
        
        $(this.$chose).click(function(){compare.setCookieOnSelectedCars();});
    },
    selectCar : function (car){
        var StockNumber = car["StockNumber"];
        if (!($.inArray(StockNumber,compare.carsSelected)!==-1)){
            compare.carsSelected.push(StockNumber);
            console.log("the car with the stock number" + StockNumber + "was selected");
            console.log("the array is now: ");
            console.log(compare.carsSelected)
        }else{
            compare.carsSelected = jQuery.grep(compare.carsSelected, function(value){
                return value !== StockNumber;
            });
            console.log(StockNumber+"was removed, here's the array:");
            console.log(compare.carsSelected);
        };
        
    },
    setCookieOnSelectedCars: function(){
        for (StockNumber of compare.carsSelected){
            document.cookie = "selection" + 0 + "=" + StockNumber + ";max-age=+10000;";
        }
        window.location.replace("appointement.html");
        
    },
    getSelectedCarFromCookie: function(){
        var selectionString = "selection"+0;
        var carSelected;
        setCarSelected = function(string){
            carSelected = compare.getCookieValueByName(string);
        };
        setCarSelected(selectionString);
        if (!carSelected){
            selectionString = " selection" + 0;
            setCarSelected(selectionString);
        }
        carSelected = carItem.search(cars,carSelected,true);
        console.log (carSelected);
        return carSelected;
        
        
    }
};
$(document).ready(function(){
    compare.init();
});